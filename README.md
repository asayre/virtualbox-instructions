For the women who code class on ansible in may:

1) Download and install virtualbox (only if you are running windows or mac):

	Download location:
	https://www.virtualbox.org/wiki/Downloads


	Install VirtualBox:
	https://www.youtube.com/watch?v=9e2_zUCaGH4


2) Download and install Linux OS ISO to virtualbox:

	Download the "DVD ISO" from here:
	https://www.centos.org/download/

	Install linux OS (Centos 7) on VirtualBox:
	https://www.youtube.com/watch?v=Gic77QS4AD4

	NOTE -  it doesn't have to be Centos 7 version of Linux OS. If you like Ubuntu or other that's fine.
	A Google search for similar instructions for other Linux OS should yield plenty of results.